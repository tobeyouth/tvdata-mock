## tvdata-mock

电视数据墙的 mock file

## 使用

- 执行 `npm install json-server -g`
- 到 `tvdata-mock`目录下执行 ` json-server --watch ./page.json --routes ./routes.json`
- 打开 `localhost:3000`就可以看到数据输出的效果了